package com.TestWithMaven;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

/**
 * Created by Brendan
 */
public class StepdefsFail {
    HomePage homePage = new HomePage();
    @Given("^User inputs all fields$")
    public void userInputesAllFieldsCorrectly() throws Throwable {
        homePage.firstname();
        homePage.surname();
        homePage.phone();
        homePage.idNumber();
    }

    @And("^does note enter a valid email$")
    public void acceptsTheTermsAndConditions() throws Throwable {
        homePage.email("brendan");
    }

    @Then("^user should not be able to request a quote$")
    public void userShouldBeAbleToRequestAQuote() throws Throwable {
        homePage.requestQuoteButton();
        homePage.emailErrorField();
    }
}
