package com.TestWithMaven;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

/**
 * Created by Brendan
 */
public class Stepdefs {
    HomePage homePage = new HomePage();
    @Given("^User inputs all fields correctly$")
    public void userInputesAllFieldsCorrectly() throws Throwable {
        System.out.println("Steps");
        homePage.firstname();
        homePage.surname();
        homePage.phone();
        homePage.idNumber();
    }

    @And("^enters a valid email$")
    public void acceptsTheTermsAndConditions() throws Throwable {
        homePage.email("brendan@automytest.com");
    }

    @Then("^user should be able to request a quote$")
    public void userShouldBeAbleToRequestAQuote() throws Throwable {
        homePage.requestQuoteButton();
    }
}
