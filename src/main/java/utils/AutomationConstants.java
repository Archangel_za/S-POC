package utils;

public class AutomationConstants {
    public static final String BROWSER_TYPE = "headless";
    public static final String URL = "https://www.santam.co.za/";
    public static final long MAX_TIMEOUTS = 30;
}
