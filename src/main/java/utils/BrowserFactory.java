package utils;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

public class BrowserFactory {
    public static WebDriver driver;

    public static WebDriver OpenBrowser(String browser, String url) {
        if (driver == null) {

            driver = startWebBrowser(browser, url);
        }
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public static void closeBrowser() {
        //driver.quit();
        driver.close();
    }

    protected static WebDriver startWebBrowser(String browser, String URL) {
        try {
            if (browser.equalsIgnoreCase("Firefox")) {
                System.out.println("local firefox started...");
                // System.setProperty("webdriver.firefox.driver", "geckodriver.exe");
                System.setProperty("webdriver.firefox.driver", "geckodriver");
                driver = new FirefoxDriver();

            } else if (browser.equalsIgnoreCase("chrome")) {
                System.out.println("local chrome started...");
                // System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
               System.setProperty("webdriver.chrome.driver", "chromedriver");
                driver = new ChromeDriver();
            } else if (browser.equalsIgnoreCase("headless")) {
                System.out.println("local phantomjs started...");
//                System.setProperty("webdriver.chrome.driver", "phantomjs.exe");
                System.setProperty("webdriver.phantomjs.driver", "phantomjs");
                DesiredCapabilities caps = new DesiredCapabilities();
                caps.setJavascriptEnabled(true);
                caps.setCapability("takesScreenshot", true);
                caps.setCapability(
                    PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
                    "phantomjs"
                );
                driver = new PhantomJSDriver(caps);
            } else
                throw new RuntimeException("Browser give " + browser + " did not load..");
        } catch (Exception e) {
            throw new RuntimeException("Browser give " + browser + " did not load..");
        }

        driver.get(URL);
        return driver;
    }

}