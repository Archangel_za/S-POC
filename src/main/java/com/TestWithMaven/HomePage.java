package com.TestWithMaven;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.*;

/**
 * Created by Brendan
 */
public class HomePage extends BasePage {
@FindBy(id = "f_name")
WebElement firstnameField;
@FindBy(id = "l_name")
WebElement surnameField;
@FindBy(id = "email")
WebElement emailField;
@FindBy(id = "phone_number")
WebElement phoneField;
@FindBy(id = "id_number")
WebElement idNumberField;
@FindBy(name = "tnc_agree")
WebElement checkBoxField;
@FindBy(id = "btn-persraq")
WebElement requestQuote;
@FindBy(id = "parsley-id-8794")
WebElement emailError;

    public void firstname() {
        firstnameField.clear();
        firstnameField.sendKeys("Brendan");
    }

    public void surname() {
        surnameField.clear();
        surnameField.sendKeys("Sahli");
    }

    public void email(String email) {
        emailField.clear();
        emailField.sendKeys(email);
    }

    public void phone() {
        phoneField.clear();
        phoneField.sendKeys("0793704512");
    }

    public void idNumber() {
        idNumberField.clear();
        idNumberField.sendKeys("9501045253080");
    }

    public void requestQuoteButton() {
        requestQuote.click();
    }

    public void emailErrorField() {
        try {
            emailError.isDisplayed();
        } catch (NoSuchElementException e) {
            System.out.println(e);
        }
    }
}
