package com.TestWithMaven;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import utils.BrowserFactory;

/**
 * Created by Brendan
 */
public class BasePage {
    WebDriver driver = BrowserFactory.getDriver();

    public BasePage()
    {
        PageFactory.initElements(driver, this);
    }
}
